<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/hamburgers/0.9.1/hamburgers.min.css">
  <link rel="stylesheet" type="text/css" href="./css/style.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- TypeKit link -->
	<link rel="stylesheet" href="https://use.typekit.net/mjo1bjp.css">
</head>

<body>

  <div class="header">

    <div class="container">

      <div class="big-header row middle-xs middle-sm middle-md middle-lg">

        <div class="logo col-md-4 col-xs-6 col-sm-6">
          <img src="./img/century-vision-global-logo.svg" />
        </div>

        <div class="col-md-8 col-xs-6 col-sm-6">
          <ul>
            <li><a href="#who-we-are">Who we are</a></li>
            <li><a href="#what-we-do">What we do</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div>

      </div>
      <!-- /big-header -->

      <div class="mobile-header row middle-xs">

        <div class="logo-small col-md-4 col-xs-6 col-sm-6">
          <img src="./img/century-vision-global-logo.svg" />
        </div>

        <div class="col-md-8 col-xs-6 col-sm-6 mobile-menu">
          <button class="hamburger hamburger--squeeze" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
          <ul class="mobile-menu-ul">
            <li><a href="#who-we-are">Who we are</a></li>
            <li><a href="#what-we-do">What we do</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div>

      </div>
      <!-- mobile-header -->

    </div>
    <!-- /container -->

  </div>
  <!-- /header -->

  <div class="hero-container section-container" style="background: linear-gradient(rgba(255, 255, 255, 0.75), rgba(255, 255, 255, 0.75)), url('./img/hero-bg.jpg') no-repeat center center/cover;">

    <div class="hero-content">
      <h1>SECURING PRACTICE LEGACIES.</h1>
      <h1>INVESTING FOR THE LONG TERM.</h1>
      <h1>EXCLUSIVELY FOCUSED ON OPHTHALMOLOGY AND OPTOMETRY.</h1>
    </div>

  </div>

  <div class="section-container" id="who-we-are" style="background: linear-gradient(rgba(255, 255, 255, 0.75), rgba(255, 255, 255, 0.75)), url('./img/section-bg-1.jpg') no-repeat center center/cover;">

    <div class="section-content">
      <div class="section-bar"></div>
      <h1>WHO WE ARE</h1>
      <p>We are a privately held, long-term investor exclusively focused on the eye care space. We acquire ophthalmic and optometric practices and ASCs but we are not a traditional private equity firm. We have no fund life cycles or outside investors. In fact, we never sell businesses and practices we acquire. We are backed by billions in assets and are seeking steady growth investments over the long term.  This approach allows us to help our partner practices grow responsibly while laying a foundation for the future.  We believe this model is the best for patients, doctors, staff members and the entire ophthalmic and optometric community.
      </p>
    </div>

  </div>

  <div class="section-container" id="what-we-do" style="background: linear-gradient(rgba(255, 255, 255, 0.75), rgba(255, 255, 255, 0.75)), url('./img/what-we-do-bg.jpg') no-repeat center center/cover;">

    <div class="section-content">
      <div class="section-bar"></div>
      <h1>WHAT WE DO</h1>
      <p>We invest in growth hungry, entrepreneurial doctors who want to build upon the legacies they have created. Our flexible, non-dilutive equity structure allows us to guarantee exit multiples, putting our partners in control of their future.</p>
      <p>
        Our slow, steady growth business model allows us to pursue a vision much bigger than creating short term economic value. Our global confederation of eye care providers will collaborate on a regional, national and global level to improve the quality of
        life of doctors, patients and staff, impact the innovation cycle in the industry and set an example for the rest of healthcare around the world.</p>
    </div>

  </div>

  <div class="section-container" style="background: #fff;" id="what-we-do">

    <div class="section-content">
      <div class="section-bar"></div>
      <h1>CENTURY VISION GLOBAL VS. PRIVATE EQUITY</h1>
      <table>
        <thead>
          <tr>
            <th scope="col">KEY FACTORS</th>
            <th scope="col">TRADITIONAL PRIVATE EQUITY</th>
            <th scope="col">CENTURY VISION GLOBAL</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td data-label="KEY FACTORS">Expected sale in 3-5 years</td>
            <td data-label="TRADITIONAL PRIVATE EQUITY">Yes</td>
            <td data-label="CENTURY VISION GLOBAL">No</td>
          </tr>
          <tr>
            <td data-label="KEY FACTORS">Depends on outside investors</td>
            <td data-label="TRADITIONAL PRIVATE EQUITY">Yes</td>
            <td data-label="CENTURY VISION GLOBAL">No</td>
          </tr>
          <tr>
            <td data-label="KEY FACTORS">Future exit multiple guarantee</td>
            <td data-label="TRADITIONAL PRIVATE EQUITY">No</td>
            <td data-label="CENTURY VISION GLOBAL">Yes</td>
          </tr>
          <tr>
            <td data-label="KEY FACTORS">Owner’s exclusive focus on eye care</td>
            <td data-label="TRADITIONAL PRIVATE EQUITY">No</td>
            <td data-label="CENTURY VISION GLOBAL">Yes</td>
          </tr>
          <tr>
            <td data-label="KEY FACTORS">Dilutive to doctors as growth occurs</td>
            <td data-label="TRADITIONAL PRIVATE EQUITY">Yes</td>
            <td data-label="CENTURY VISION GLOBAL">No</td>
          </tr>
          <tr>
            <td data-label="KEY FACTORS">Model conducive to younger doctors</td>
            <td data-label="TRADITIONAL PRIVATE EQUITY">No</td>
            <td data-label="CENTURY VISION GLOBAL">Yes</td>
          </tr>
          <tr>
            <td data-label="KEY FACTORS">Geographic control</td>
            <td data-label="TRADITIONAL PRIVATE EQUITY">Sometimes</td>
            <td data-label="CENTURY VISION GLOBAL">Always</td>
          </tr>
          <tr>
            <td data-label="KEY FACTORS">Future commitment known</td>
            <td data-label="TRADITIONAL PRIVATE EQUITY">No</td>
            <td data-label="CENTURY VISION GLOBAL">Yes</td>
          </tr>
        </tbody>

      </table>
    </div>

  </div>

  <div class="section-container" style="background: #fff;" id="contact">

    <div class="section-content">
      <div class="section-bar"></div>
      <h1>CONTACT</h1>
      <p>Please fill out the form below and a Century Vision Global consultant will contact you. We look forward to exploring a relationship.</p>
      <div class="row form-row middle-xs">
        <div class="col-md-6 col-xs-12 col-sm-12 direct-contact">
          <div class="phone-container contact-container">
            <a class="phone-link" href="tel:8889839322"><i class="fa fa-phone" aria-hidden="true"></i>(888) 983-9322</a>
          </div>
          <div class="email-container contact-container">
            <i class="fa fa-envelope-o" aria-hidden="true"></i>buildwithus@cvgloballlc.com
          </div>
        </div>
        <!-- col-md-6 -->
        <div class="col-md-6 col-xs-12 col-sm-12">
          <form id="contact-form" method="post" role="form">
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12">
                <input name="name" id="name" placeholder="Name" data-validation="length" data-validation-length="min4" required="required"></input>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12">
                <input name="practice" id="practice" placeholder="Practice name"></input>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12">
                <input name="city-state" id="city-state" placeholder="City, State" required="required" data-validation="length" data-validation-length="min4"></input>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="styled-select">
                  <select name="position-select">
                  <option value="administrative">Administrative</option>
                  <option value="physician-shareholder">Physician Shareholder</option>
                  <option value="other">Other</option>
                </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-12 col-sm-12 two-up">
                <input name="phone" id="phone" placeholder="(555)555-5555" required="required" data-validation="custom" data-validation-regexp="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$" data-validation-error-msg="Invalid Phone Number"></input>
              </div>
              <div class="col-md-6 col-xs-12 col-sm-12 two-up">
                <input name="email" id="email" placeholder="Email" data-validation="email" required="required"></input>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12">
                <input type="submit" value="SUBMIT" id="submit">
              </div>
            </div>
            <div class="messages"></div>
          </form>
        </div>
        <!-- col-md-6 -->
      </div>
    </div>

  </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.77/jquery.form-validator.min.js">
</script>

<script src="./js/scripts.js?2"></script>

</body>
</html>
