<?php
session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST') {

  $pass = $_POST['pass'];

  if($pass == "buildwithus2018") {

    $_SESSION['loggedin'] = true;

    header("Location:requestform.php");
    exit;

  }

}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/hamburgers/0.9.1/hamburgers.min.css">
  <link rel="stylesheet" type="text/css" href="./css/style.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- TypeKit link -->
	<link rel="stylesheet" href="https://use.typekit.net/mjo1bjp.css">
</head>

<body>

  <div class="header">

    <div class="container">

      <div class="big-header row middle-xs middle-sm middle-md middle-lg">

        <div class="logo col-md-4 col-xs-6 col-sm-6">
          <img src="./img/century-vision-global-logo.svg" />
        </div>

        <div class="col-md-8 col-xs-6 col-sm-6">
          <ul>
            <li><a href="#who-we-are">Who we are</a></li>
            <li><a href="#what-we-do">What we do</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div>

      </div>
      <!-- /big-header -->

      <div class="mobile-header row middle-xs">

        <div class="logo-small col-md-4 col-xs-6 col-sm-6">
          <img src="./img/century-vision-global-logo.svg" />
        </div>

        <div class="col-md-8 col-xs-6 col-sm-6 mobile-menu">
          <button class="hamburger hamburger--squeeze" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
          <ul class="mobile-menu-ul">
            <li><a href="#who-we-are">Who we are</a></li>
            <li><a href="#what-we-do">What we do</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div>

      </div>
      <!-- mobile-header -->

    </div>
    <!-- /container -->

  </div>
  <!-- /header -->
<div class="section-container">
  <div class="row center-xs">
    <div class="col-xs-12">
      <div class="form-heading">To access this form, please enter the password.</div>
      <form method="post">
        <div>
          <input type="password" name="pass"></input>
        </div>
        <div>
          <input type="submit" value="Submit"></input>
        </div>
      </form>
    </div>
  </div>
</div>
</body>
</html>
