<?php

    // an email address that will be in the From field of the email.
    $from = 'reply@centuryvisionglobal.com';

    // an email address that will receive the email with the output of the form
    $sendTo = 'chris.moore@cvgloballlc.com';

    // subject of the email
    $subject = 'CenturyVisionGlobal - Private Form Submission';

    // form field names and their translations.
    // array variable name => Text to appear in the email
    $fields = array(
      'owner-name' => 'Owner Name',
      'percent-ownership' => '% Ownership',
      'opthalmologist-number' => '<b>Opthalmologist Number</b>',
      'optometrist-number' => '<b>Optometrist Number</b>',
      'clinic-number' => '<b>Number of Clinics</b>',
      'surgery-center' => 'Surgery Center Name',
      'surgery-center-city-state' => 'Surgery Center City/State',
      'percent-ASC-ownership' => 'Percent ASC Ownership',
      'legal-info' => '<b>Legal Info</b>',
      'expense-info' => '<b>Expense Info</b>'
    );


    // message that will be displayed when everything is OK :)
    $okMessage = 'Thank you for your inquiry. A representative from Century Vision Global will be in touch soon.';

    // If something goes wrong, we will display this message.
    $errorMessage = 'There was an error while submitting the form. Please try again later.';

    /*
     *  LET'S DO THE SENDING
     */

    // if you are not debugging and don't need error reporting, turn this off by error_reporting(0);
    //error_reporting(0);
    // print_r($_POST['surgery-center']);
    // exit;

    try
    {

        if(count($_POST) == 0) throw new \Exception('Form is empty');

        $emailText = "You have a new message from your contact form<br>=============================<br>";

        foreach ($_POST as $key => $value) {
            // If the field exists in the $fields array, include it in the email
            if (isset($fields[$key])) {
              if ($key == 'owner-name') {
                $emailText .= "<b>Owner Name, Percent Ownership:</b><br> \n";
                foreach($_POST['owner-name']['name'] as $i => $name) {
                  $emailText .= $name . ', ' . $_POST['owner-name']['percent'][$i] . "% <br>";
                }
              } elseif ($key == 'surgery-center') {
                $emailText .= "<b>Surgery Center, Surgery Location, Percent ASC:</b> <br>";
                foreach($_POST['surgery-center']['name'] as $k => $surgeryName) {
                  $emailText .= $surgeryName . ', ' . $_POST['surgery-center']['location'][$k] . ', ' . $_POST['surgery-center']['percent-ASC'][$k] . "% <br>";
                }
              } else {
                $emailText .= "$fields[$key]: $value\n" . "<br>";
              }
            }
        }

        // All the neccessary headers for the email.
        $headers = array('Content-Type: text/html; charset="UTF-8";',
            'From: ' . $from,
            'Reply-To: ' . $from,
            'Return-Path: ' . $from,
        );

        // Send email
        mail($sendTo, $subject, $emailText, implode("\n", $headers));

        $responseArray = array('type' => 'success', 'message' => $okMessage);
    }
    catch (\Exception $e)
    {
        $responseArray = array('type' => 'danger', 'message' => $errorMessage);
    }


    // if requested by AJAX request return JSON response
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $encoded = json_encode($responseArray);

        header('Content-Type: application/json');

        echo $encoded;
    }
    // else just display the message
    else {
        echo $responseArray['message'];
    }

?>
