//If scrolled, attach white background to header and shrink
$(window).scroll(function() {

  var scroll = $(window).scrollTop();

  if (scroll >= 100) {
    $('.header').addClass('scrolled');
    $('.logo img').addClass('scrolled-logo');
    $('.logo-small img').addClass('scrolled-logo');
  } else {
    $('.header').removeClass('scrolled');
    $('.logo img').removeClass('scrolled-logo');
    $('.logo-small img').removeClass('scrolled-logo');
  }

});

$( ".header a" ).click(function( event ) {
      event.preventDefault();
      $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 500);
});


$('.hamburger').click(function() {
  $(this).toggleClass('is-active');
  $(this).siblings('.mobile-menu-ul').slideToggle("fast");
})


//Form validation
$.validate({
  validateOnBlur : true,
  showHelpOnFocus : false
});

// when the form is submitted
$('#contact-form').on('submit', function (e) {

    // if the validator does not prevent form submit
    if (!e.isDefaultPrevented()) {
        e.preventDefault();
        var url = "../contact-submit.php";

        // POST values in the background the the script URL
        $.ajax({
            type: "POST",
            url: url,
            // data = JSON object that contact-submit.php returns
            data: $(this).serialize(),
            success: function (data)
            {
                // receive the type of the message: success x danger and apply it
                var messageAlert = 'alert-' + data.type;
                var messageText = data.message;

                // Alert box html
                var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable">' + messageText + '</div>';

                // If have messageAlert and messageText
                if (messageAlert && messageText) {
                    // inject the alert to .messages div in our form
                    $('#contact-form').find('.messages').html(alertBox);
                    // empty the form
                    $('#contact-form')[0].reset();
                }
            }
        });
        return false;
    }
});
