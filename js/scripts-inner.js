//If scrolled, attach white background to header and shrink
$(window).scroll(function() {

  var scroll = $(window).scrollTop();

  if (scroll >= 100) {
    $('.header').addClass('scrolled');
    $('.logo img').addClass('scrolled-logo');
    $('.logo-small img').addClass('scrolled-logo');
  } else {
    $('.header').removeClass('scrolled');
    $('.logo img').removeClass('scrolled-logo');
    $('.logo-small img').removeClass('scrolled-logo');
  }

});

// $( ".header a" ).click(function( event ) {
//       event.preventDefault();
//       $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 500);
// });


$('.hamburger').click(function() {
  $(this).toggleClass('is-active');
  $(this).siblings('.mobile-menu-ul').slideToggle("fast");
})
