<?php

  session_start();

  if (empty($_SESSION['loggedin'])) {
    header("Location:initialrequestform.php");
    exit;
  }

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/hamburgers/0.9.1/hamburgers.min.css">
  <link rel="stylesheet" type="text/css" href="./css/style.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- TypeKit link -->
	<link rel="stylesheet" href="https://use.typekit.net/mjo1bjp.css">
</head>

<body>

  <div class="header">

    <div class="container">

      <div class="big-header row middle-xs middle-sm middle-md middle-lg">

        <div class="logo col-md-4 col-xs-6 col-sm-6">
          <img src="./img/century-vision-global-logo.svg" />
        </div>

        <div class="col-md-8 col-xs-6 col-sm-6">
          <ul>
            <li><a href="./index.php#who-we-are">Who we are</a></li>
            <li><a href="./index.php#what-we-do">What we do</a></li>
            <li><a href="./index.php#contact">Contact</a></li>
          </ul>
        </div>

      </div>
      <!-- /big-header -->

      <div class="mobile-header row middle-xs">

        <div class="logo-small col-md-4 col-xs-6 col-sm-6">
          <img src="./img/century-vision-global-logo.svg" />
        </div>

        <div class="col-md-8 col-xs-6 col-sm-6 mobile-menu">
          <button class="hamburger hamburger--squeeze" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
          <ul class="mobile-menu-ul">
            <li><a href="./index.php#who-we-are">Who we are</a></li>
            <li><a href="./index.php#what-we-do">What we do</a></li>
            <li><a href="./index.php#contact">Contact</a></li>
          </ul>
        </div>

      </div>
      <!-- mobile-header -->

    </div>
    <!-- /container -->

  </div>
  <!-- /header -->

  <div class="section-container" style="background: #fff;" id="contact">

    <div class="section-content private-form-content">
      <div class="section-bar"></div>
      <h1>PRIVATE CONTACT FORM</h1>
      <p>Please fill out the form below and a Century Vision Global consultant will contact you. We look forward to exploring a relationship.</p>
      <div class="row form-row middle-xs">
        <!-- col-md-6 -->
        <div class="col-xs-12">
          <form id="private-form" method="post" role="form">
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12">
                <input name="practice-legal-name" id="practice-legal-name" placeholder="Practice legal name" data-validation="length" data-validation-length="min4" required="required"></input>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12">
                <input name="practice-address" id="practice-address" placeholder="Address of practice location"></input>
              </div>
            </div>
            <div class="divider"></div>
            <div class="row center-xs">
              <div class="row parent-form-row parent-owner-row">
                <div class=" col-xs-12 form-heading">Owners names and % of ownership? (List all)</div>
                <div class="row inner-form-row inner-owner-row">
                  <div class="col-md-6 col-xs-12 col-sm-6 two-up">
                    <input name="owner-name[name][]" placeholder="Owner name"></input>
                  </div>
                  <div class="col-md-6 col-xs-12 col-sm-6 two-up">
                    <input name="owner-name[percent][]" placeholder="% of ownership"></input>
                  </div>
                </div>
              </div>
              <div class="add-btn add-owner-btn btn">+ Add another</div>
              <div class="remove-btn remove-owner-btn btn">- Remove last</div>
            </div>
            <div class="divider"></div>
            <div class="row">
              <div class="col-md-4 col-xs-12 col-sm-12">
                <input name="opthalmologist-number" id="opthalmologist-number" placeholder="# of opthalmologists" required="required" data-validation="number"></input>
              </div>
              <div class="col-md-4 col-xs-12 col-sm-12">
                <input name="optometrist-number" id="optometrist-number" placeholder="# of optometrists" required="required" data-validation="number"></input>
              </div>
              <div class="col-md-4 col-xs-12 col-sm-12">
                <input name="clinic-number" id="clinic-number" placeholder="# of clinic locations" required="required" data-validation="number"></input>
              </div>
            </div>
            <div class="divider"></div>
            <div class="row parent-form-row parent-surgery-row">
              <div class=" col-xs-12 form-heading">Where are surgeries performed? (List all locations)</div>
              <div class="row inner-form-row inner-surgery-row">
                <div class="col-md-4 col-xs-12 col-sm-12 two-up">
                  <input name="surgery-center[name][]" placeholder="Surgery center name"></input>
                </div>
                <div class="col-md-4 col-xs-12 col-sm-12 two-up">
                  <input name="surgery-center[location][]" placeholder="City, State"></input>
                </div>
                <div class="col-md-4 col-xs-12 col-sm-12 two-up">
                  <input name="surgery-center[percent-ASC][]" placeholder="% of ASC ownership"></input>
                </div>
              </div>
            </div>
            <div class="add-btn add-surgery-btn btn">+ Add another</div>
            <div class="remove-btn remove-surgery-btn btn">- Remove last</div>
            <div class="divider"></div>
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12">
                <div class=" col-xs-12 form-heading">Legal background</div>
                <textarea name="legal-info" id="legal-info" placeholder="Are there any judgments, liens, or pending lawsuits against the practice or any of its providers? If yes, please explain."></textarea>
              </div>
            </div>
            <div class="divider"></div>
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12">
                <div class=" col-xs-12 form-heading">Expense background</div>
                <textarea name="expense-info" id="legal-info" placeholder="Were there any major one time abnormal expenses that occurred in the last year? If so, please provide details and approximate amount of the expense."></textarea>
              </div>
            </div>
            <div class="divider"></div>
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12">
                <input type="submit" value="Submit" id="submit">
              </div>
            </div>
            <div class="messages"></div>
          </form>
        </div>
        <!-- col-md-6 -->
      </div>
    </div>

  </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.77/jquery.form-validator.min.js">
</script>

<script src="./js/scripts-inner.js?2"></script>
<script>

  $('.add-owner-btn').click(function() {
    $('.inner-owner-row:first').clone().addClass('add-on').find("input:text").val("").end().appendTo('.parent-owner-row');
  });

  $('.remove-owner-btn').click(function() {
    if ($('.inner-owner-row:last').hasClass('add-on')) {
      $('.inner-owner-row:last').remove();
    }
  });

  $('.add-surgery-btn').click(function() {
    $('.inner-surgery-row:first').clone().addClass('add-on').find("input:text").val("").end().appendTo('.parent-surgery-row');
  });

  $('.remove-surgery-btn').click(function() {
    if ($('.inner-surgery-row:last').hasClass('add-on')) {
      $('.inner-surgery-row:last').remove();
    }
  });

  // when the form is submitted
  $('#private-form').on('submit', function (e) {

      // if the validator does not prevent form submit
      if (!e.isDefaultPrevented()) {
          e.preventDefault();
          var url = "/private-submit.php";

          // POST values in the background the the script URL
          $.ajax({
              type: "POST",
              url: url,
              // data = JSON object that contact-submit.php returns
              data: $(this).serialize(),
              success: function (data)
              {
                  // receive the type of the message: success x danger and apply it
                  var messageAlert = 'alert-' + data.type;
                  var messageText = data.message;

                  // Alert box html
                  var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable">' + messageText + '</div>';

                  // If have messageAlert and messageText
                  if (messageAlert && messageText) {
                      // inject the alert to .messages div in our form
                      $('#private-form').find('.messages').html(alertBox);
                      // empty the form
                      $('#private-form')[0].reset();
                  }
              }
          });
          return false;
      }
  });

</script>
</body>
</html>
